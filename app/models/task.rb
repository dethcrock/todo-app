# == Schema Information
#
# Table name: tasks
#
#  id         :integer          not null, primary key
#  title      :string
#  body       :text
#  remind_at  :datetime
#  priority   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Task < ApplicationRecord

  has_many :subtasks
  accepts_nested_attributes_for :subtasks

  enum priority: {
    low: 0,
    medium: 1,
    high: 2
  } 

end
