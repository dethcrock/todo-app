# == Schema Information
#
# Table name: subtasks
#
#  id         :integer          not null, primary key
#  title      :string
#  done       :boolean
#  task_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Subtask < ApplicationRecord

  belongs_to :task

end
