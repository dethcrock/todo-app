class SubtasksController < ApplicationController

   skip_before_action :verify_authenticity_token

  def create
    @subtask = Subtask.new(subtask_params)
    respond_to do |format|
      format.js do
        if @subtask.save
          render json: { id: @subtask.id }
        end
      end
    end
  end

  def finished
    @subtask = Subtask.find(params[:id])
    @subtask.update(done: params[:done])
  end

  private

  def subtask_params
    {
      title: params[:title],
      task_id: params[:task_id]
    }
  end
end
