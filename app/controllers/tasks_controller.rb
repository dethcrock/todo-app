class TasksController < ApplicationController

  before_action :task, only: %i[show edit update destroy]

  def index
    @tasks = Task.all
  end

  def show
    render :edit
  end

  def new
    @task = Task.new
  end

  def edit; end

  def create
    @task = Task.create(task_params)
    redirect_to edit_task_path(@task)
  end

  def update
    if @task.update(task_params)
      redirect_to root_path
    else
      flash[:alert] = 'Somthing wrong, Record not save!'
    end
  end

  def destroy
    @task.destroy
    redirect_to root_path
  end

  private

  def task_params
    params.require(:task).permit(
      %i[
        id
        title
        body
        priority
        remind_at
      ]
    )
  end

  def task
    @task = Task.find(params[:id])
  end

end
