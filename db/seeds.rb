# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

tasks = Task.create([
  {
    title: 'Test task',
    body: 'Test task body',
    priority: :high,
  }
])

subtask = Subtask.create(task_id: tasks.first.id, title: 'Subtask')
