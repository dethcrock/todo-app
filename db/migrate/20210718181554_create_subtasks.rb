class CreateSubtasks < ActiveRecord::Migration[6.1]
  def change
    create_table :subtasks do |t|
      t.string :title
      t.boolean :done, default: false
      t.belongs_to :task

      t.timestamps
    end
  end
end
