class CreateTasks < ActiveRecord::Migration[6.1]
  def change
    create_table :tasks do |t|
      t.string :title
      t.text :body
      t.datetime :remind_at
      t.integer :priority

      t.timestamps
    end
  end
end
