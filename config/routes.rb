Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'tasks#index'
  resources :tasks, except: %i[index]
  resources :subtasks, only: %i[create edit destroy]do
    patch :finished, on: :member
  end
end
